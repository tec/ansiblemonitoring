# Ansible Monitoring

### License

Copyright © 2019–2020 by Timothy Chapman <tec@ucc.gu.uwa.edu.au>
and Nick Bannon <nick@ucc.gu.uwa.edu.au>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

### The general idea

Monitoring on servers is good. A consistent setup across serversis good.
Ansible should be a good delivery mechanism for this.

### Monitoring

Currently (my) thoughts are that the following combination looks promising:
 - Grafana
 - Prometheus
 - Thanos
 
 There seems to be ansible scripts for installing these individual components
  - https://github.com/cloudalchemy/ansible-grafana
  - https://github.com/cloudalchemy/ansible-prometheus
  - https://github.com/transferwise/ansible-thanos
