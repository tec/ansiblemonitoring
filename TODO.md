## meetings.ucc.asn.au , https://docs.bigbluebutton.org/
 - mongo mongodb://127.0.1.1:27017/meteor --eval "db.users.count({connectionStatus:'online'})"
 - mongo mongodb://127.0.1.1:27017/meteor --eval "db.meetings.find()" | wc -l
 - /opt/freeswitch/bin/fs_cli -x "show channels"
 - or:
   - https://github.com/greenstatic/bigbluebutton-exporter

## all hosts
 - temperatures in the clubroom
 - DNS queries per second
 - number of active and available DHCP leases on the loft/wireless/clubroom networks
 - disk latency, display in dashboard?
 - TCP SYN rate?
 - outbound SSH login attempts?
 - inbound SSH login attempts?
